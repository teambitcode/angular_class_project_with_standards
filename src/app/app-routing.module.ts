import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/login/login.component';
import { landingChildRoutes, LandingComponent } from './landing/landing.component';
const routes: Routes = [
  {
    path:'',component: LoginComponent
  },
  {
    path:'login',component: LoginComponent
  }
  ,
  {
    path:'landing',component: LandingComponent,
    children: landingChildRoutes
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
