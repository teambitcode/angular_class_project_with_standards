import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MainService } from '../main.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userData:any[] = [];
  public loginForm: FormGroup;
  public registerForm: FormGroup;
  isLogin: boolean = true;
  constructor(private router: Router,public apiService: MainService, private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.getAllUsers();
    this.loginForm = this.formBuilder.group({
      'email': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });

    this.registerForm = this.formBuilder.group({
      'email': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])],
      'confPassword': [null, Validators.compose([Validators.required])],
      'firstName': [null, Validators.compose([Validators.required])],
      'lastName': [null, Validators.compose([Validators.required])]
    });
  }
  getAllUsers() {
    console.log("***************8");
    this.apiService.getApi('/user/getAll').subscribe((response: any) => {
      this.userData = response.data;
      console.log(this.userData.length);
    },
    (errorValue:any)=>{
      console.log(errorValue);
    });
  }

  loginUser(){
    let tempBody = {
      "email": this.loginForm.get('email').value,
      "password": this.loginForm.get('password').value
    };
    this.apiService.postApi('/user/login',tempBody).subscribe((response: any) => {
      console.log(response);
      this.router.navigateByUrl('/landing/home');
    },
    (errorValue:any)=>{
      console.log(errorValue.error.msg);
    });
  }

  registerUser(){


    let passwrd = this.registerForm.get('password').value;
    let confPassword = this.registerForm.get('confPassword').value

    if(passwrd === confPassword){
      let tempBody = {
        "role":"user",
        "email": this.registerForm.get('email').value,
        "password": this.registerForm.get('password').value,
        "first_name":this.registerForm.get('firstName').value,
        "last_name":this.registerForm.get('lastName').value,
      };

      this.apiService.postApi('/user/new',tempBody).subscribe((response: any) => {
        console.log(response);
      },
      (errorValue:any)=>{
        console.log(errorValue.error.msg);
      });

    }else{
      console.log("Password not matched");

    }

    
  }

  gotoRegister(){
    this.isLogin = false;
  }

  gotoLogin(){
    this.isLogin = true;
  }
}
