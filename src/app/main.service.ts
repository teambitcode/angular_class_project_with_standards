import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class MainService {
    private email:string = '';
    constructor(private http: HttpClient) { }

    // Perform a GET Request
    getApi(path: string): Observable<any> {
        return this.http.get(environment.basePath+path);
    }

    postApi(path: string, body:any): Observable<any> {
        return this.http.post(environment.basePath+path,body);
    }

    deleteApi(path: string,id:string): Observable<any> {
        return this.http.get(environment.basePath+path+"?id="+id);
    }

}