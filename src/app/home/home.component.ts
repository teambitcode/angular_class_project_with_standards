import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userData:any[] = [];
  constructor(public apiService: MainService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  
  getAllUsers(){
    this.apiService.getApi('/user/getAll').subscribe((response:any)=>{
      this.userData = response.data;
    },
   (errorData:any)=>{
     console.log(errorData)
   } );
  }


}
